#define VMA_IMPLEMENTATION
#include <cstdio>
#include <vulkan/vulkan.h>
#include <vk_mem_alloc.h>
#include <functional>
#include <iostream>
#include <vector>
#include <thread>
#include <random>

#define CHECK_VK_RESULT(_expr) \
result = _expr; \
if (result != VK_SUCCESS) \
{ \
    cerr << "Error executing " << #_expr << ": " << result << endl; \
}

#define GET_EXTENSION_FUNCTION(_id) reinterpret_cast<PFN_##_id>(vkGetInstanceProcAddr(instance, #_id))

using namespace std;

struct VmaBuffer
{
    VmaBuffer(VmaAllocator allocator, size_t count, VmaMemoryUsage usage)
        : allocator(allocator)
    {
        VkResult result;

        VkBufferCreateInfo createInfo{};
        createInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
        createInfo.size = count * sizeof(float);
        createInfo.usage = VK_BUFFER_USAGE_STORAGE_BUFFER_BIT;
        createInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

        VmaAllocationCreateInfo allocateInfo{};
        allocateInfo.usage = usage;

        CHECK_VK_RESULT(vmaCreateBuffer(allocator, &createInfo, &allocateInfo, &buffer, &allocation, nullptr));
    }
    VmaBuffer(const VmaBuffer& rhs) = delete;
    VmaBuffer(VmaBuffer&& rhs) = delete;
    ~VmaBuffer()
    {
        vmaDestroyBuffer(allocator, buffer, allocation);
    }

    void map(const function<void(float*)>& behavior)
    {
        VkResult result;

        float* data;
        CHECK_VK_RESULT(vmaMapMemory(allocator, allocation, reinterpret_cast<void**>(&data)));

        behavior(data);

        vmaUnmapMemory(allocator, allocation);
    }

    VmaBuffer& operator=(const VmaBuffer& rhs) = delete;
    VmaBuffer& operator=(VmaBuffer&& rhs) = delete;

    VkBuffer buffer;
    VmaAllocation allocation;

private:
    VmaAllocator allocator;
};

static VkBool32 onError(
    VkDebugUtilsMessageSeverityFlagBitsEXT severity,
    VkDebugUtilsMessageTypeFlagsEXT type,
    const VkDebugUtilsMessengerCallbackDataEXT* callbackData,
    void* userData
)
{
    cerr << "Vulkan ";

    switch (type)
    {
    case VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT :
        cerr << "general ";
        break;
    case VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT :
        cerr << "validation ";
        break;
    case VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT :
        cerr << "performance ";
        break;
    }

    switch (severity)
    {
    case VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT :
        cerr << "(verbose): ";
        break;
    default :
    case VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT :
        cerr << "(info): ";
        break;
    case VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT :
        cerr << "(warning): ";
        break;
    case VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT :
        cerr << "(error): ";
        break;
    }

    cerr << callbackData->pMessage << endl;

    return false;
}

int main(int argc, char** argv)
{
    VkResult result;

    const char* const programName = "Bug Demo";
    VkInstance instance;
    VkDebugUtilsMessengerEXT debugMessenger;
    VkPhysicalDevice physDevice;
    uint32_t queueFamilyIndex = 0;
    VkDevice device;
    VkQueue queue;
    VmaAllocator allocator;

    {
        vector<const char*> extensionNames = { "VK_EXT_debug_utils" };
        vector<const char*> layerNames = { "VK_LAYER_KHRONOS_validation" };

        VkApplicationInfo applicationInfo{};
        applicationInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
        applicationInfo.pApplicationName = programName;
        applicationInfo.applicationVersion = VK_MAKE_VERSION(0, 1, 0);
        applicationInfo.pEngineName = programName;
        applicationInfo.engineVersion = VK_MAKE_VERSION(0, 1, 0);
        applicationInfo.apiVersion = VK_API_VERSION_1_3;

        VkInstanceCreateInfo createInfo{};
        createInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
        createInfo.pApplicationInfo = &applicationInfo;
        createInfo.enabledExtensionCount = extensionNames.size();
        createInfo.ppEnabledExtensionNames = extensionNames.data();
        createInfo.enabledLayerCount = layerNames.size();
        createInfo.ppEnabledLayerNames = layerNames.data();

        CHECK_VK_RESULT(vkCreateInstance(&createInfo, nullptr, &instance));
    }

    {
        VkDebugUtilsMessengerCreateInfoEXT createInfo{};
        createInfo.sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT;
        createInfo.messageSeverity = VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT;
        createInfo.messageType = VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT;
        createInfo.pfnUserCallback = onError;

        CHECK_VK_RESULT(GET_EXTENSION_FUNCTION(vkCreateDebugUtilsMessengerEXT)(instance, &createInfo, nullptr, &debugMessenger));
    }

    uint32_t physDeviceCount;
    vkEnumeratePhysicalDevices(instance, &physDeviceCount, nullptr);

    vector<VkPhysicalDevice> physDevices(physDeviceCount);
    vkEnumeratePhysicalDevices(instance, &physDeviceCount, physDevices.data());

    uint32_t bestScore = 0;

    for (VkPhysicalDevice device : physDevices)
    {
        VkPhysicalDeviceProperties properties{};
        vkGetPhysicalDeviceProperties(device, &properties);

        uint32_t score;

        switch (properties.deviceType)
        {
        default :
            continue;
        case VK_PHYSICAL_DEVICE_TYPE_OTHER :
            score = 1;
            break;
        case VK_PHYSICAL_DEVICE_TYPE_INTEGRATED_GPU :
            score = 4;
            break;
        case VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU :
            score = 5;
            break;
        case VK_PHYSICAL_DEVICE_TYPE_VIRTUAL_GPU :
            score = 3;
            break;
        case VK_PHYSICAL_DEVICE_TYPE_CPU :
            score = 2;
            break;
        }

        if (score > bestScore)
        {
            physDevice = device;
            bestScore = score;
        }
    }

    {
        vector<const char*> extensionNames;
        vector<const char*> layerNames = { "VK_LAYER_KHRONOS_validation" };

        uint32_t queueFamilyCount;
        vkGetPhysicalDeviceQueueFamilyProperties(physDevice, &queueFamilyCount, nullptr);

        vector<VkQueueFamilyProperties> queueFamilies(queueFamilyCount);
        vkGetPhysicalDeviceQueueFamilyProperties(physDevice, &queueFamilyCount, queueFamilies.data());

        for (uint32_t i = 0; i < queueFamilyCount; i++)
        {
            if ((queueFamilies[i].queueFlags & VK_QUEUE_COMPUTE_BIT))
            {
                queueFamilyIndex = i;
                break;
            }
        }
        
        float priority = 1;

        VkDeviceQueueCreateInfo queueCreateInfo{};
        queueCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
        queueCreateInfo.queueFamilyIndex = queueFamilyIndex;
        queueCreateInfo.queueCount = 1;
        queueCreateInfo.pQueuePriorities = &priority;

        VkPhysicalDeviceFeatures2 features{};
        features.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FEATURES_2;

        VkDeviceCreateInfo createInfo{};
        createInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
        createInfo.pNext = &features;
        createInfo.queueCreateInfoCount = 1;
        createInfo.pQueueCreateInfos = &queueCreateInfo;
        createInfo.enabledExtensionCount = extensionNames.size();
        createInfo.ppEnabledExtensionNames = extensionNames.data();
        createInfo.enabledLayerCount = layerNames.size();
        createInfo.ppEnabledLayerNames = layerNames.data();

        CHECK_VK_RESULT(vkCreateDevice(physDevice, &createInfo, nullptr, &device));

        vkGetDeviceQueue(device, queueFamilyIndex, 0, &queue);
    }

    {
        VmaAllocatorCreateInfo createInfo{};
        createInfo.physicalDevice = physDevice;
        createInfo.device = device;
        createInfo.instance = instance;

        CHECK_VK_RESULT(vmaCreateAllocator(&createInfo, &allocator));
    }

    int threadCount = 4;
    thread workers[threadCount];

    for (int i = 0; i < threadCount; i++)
    {
        workers[i] = thread([&]() -> void {
            random_device device;
            mt19937 engine(device());
            uniform_int_distribution<int> dist(0, 1);
            uniform_real_distribution<float> dataDist(0, 1);

            for (int i = 0; i < 1'000'000; i++)
            {
                bool dir = dist(engine);

                int count = 1024;
                VmaBuffer buffer(allocator, count, dir ? VMA_MEMORY_USAGE_CPU_TO_GPU : VMA_MEMORY_USAGE_GPU_TO_CPU);

                if (dir)
                {
                    float srcData[count];

                    for (int i = 0; i < count; i++)
                    {
                        srcData[i] = dataDist(engine);
                    }

                    buffer.map([&](float* data) -> void {
                        memcpy(data, srcData, count * sizeof(float));
                    });
                }
                else
                {
                    float dstData[count];

                    buffer.map([&](float* data) -> void {
                        memcpy(dstData, data, count * sizeof(float));
                    });
                }
            }
        });
    }

    for (int i = 0; i < threadCount; i++)
    {
        workers[i].join();
    }

    CHECK_VK_RESULT(vkDeviceWaitIdle(device));

    vmaDestroyAllocator(allocator);
    vkDestroyDevice(device, nullptr);
    GET_EXTENSION_FUNCTION(vkDestroyDebugUtilsMessengerEXT)(instance, debugMessenger, nullptr);
    vkDestroyInstance(instance, nullptr);

    return 0;
}
